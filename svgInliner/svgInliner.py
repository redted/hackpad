from lxml import etree
import os.path
import sys

def inlineSvg(path, outputPath):
	parser = etree.XMLParser(recover=True)
	tree = etree.parse(path, parser)
	
	root = tree.getroot()
	
	#can't add a namespace
	#root.set('xmlns:xlink', 'http://www.w3.org/1999/xlink')

	nsmap = {'xlink': 'http://www.w3.org/1999/xlink'}
	
	etree.register_namespace('xlink','http://www.w3.org/1999/xlink')
	
	inlinedFiles = set()
	
	svgParentFolder = os.path.dirname(path)

	#for img in tree.iterfind('image', namespaces=root.nsmap):
	for img in tree.iter('{http://www.w3.org/2000/svg}image'):
		#lxml: we have to translate xlink: to {http://www.w3.org/1999/xlink} for it to work, despit the lib returning xlink: ...
		settableAttributes = dict(img.items()) #img.attribute
		settableAttributes['{http://www.w3.org/1999/xlink}href'] = settableAttributes['xlink:href']

		#treat all svg paths as relative to the current folder
		pathSvg = os.path.join(svgParentFolder, settableAttributes['xlink:href'].lstrip('\\/'))
		svgId   = os.path.splitext(os.path.basename(pathSvg))[0]
		
		del settableAttributes['xlink:href']
		
		if pathSvg.endswith('.svg'):
			if not pathSvg in inlinedFiles:
				inlineTree = etree.parse(pathSvg, parser)
				
				symbol = img.makeelement('symbol', id=svgId)
				
				symbol.extend(inlineTree.getroot())
				
				root.insert(0, symbol)
				
				inlinedFiles.add(pathSvg)
				
				print('Embedded '+svgId)
			
			settableAttributes['{http://www.w3.org/1999/xlink}href'] = '#'+svgId
		
		#print(etree.tostring(img.makeelement('use', attrib=img.attrib, nsmap=nsmap)))
		img.getparent().replace(img, img.makeelement('use', settableAttributes))
		#print(etree.tostring(img.makeelement('use', settableAttributes)))
		#print(etree.tostring(img.makeelement('use', attrib=img.attrib, nsmap=nsmap)))

	file=open(outputPath,'wb')
	file.write(etree.tostring(tree, pretty_print=True))
	file.close()
	
	return tree #.getroot()

if __name__ == "__main__":
	if len(sys.argv) == 2:
		filename, extension = os.path.splitext(os.path.basename(sys.argv[1]))
		convertedFilename = os.path.join(os.path.dirname(sys.argv[1]), filename + '-selfcontained'+extension)
		inlineSvg(sys.argv[1], convertedFilename)
	elif len(sys.argv) == 3:
		inlineSvg(sys.argv[1], sys.argv[2])
	else:
		print('Attemtps to inline svg files. Usage: python '+sys.argv[0]+' input.svg [output.svg]\n\nIf output.svg is ommitted -selfcontained will be attached to the filename. Warning:overwrites files without warning.')