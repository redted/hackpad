#!/bin/sh

# Below is a flow to generate svg files for the pgadmin4 explain graphics. It is kept generic but targets the 22 tpch queries.
#

# 1. Manually put your query through pgadmin 4
# 2. Use this bookmarklet (create a bookmark with this url) to copy the svg to the clipboard, alternativly use the save as bookmarklet
#
#     Bookmarklet copy to clipboard: "javascript:navigator.clipboard.writeText(document.evaluate('.//*[@id="1"]/table/tbody/tr/td/div/div/div[2]', document.getElementsByTagName('iframe')[0].contentDocument, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerHTML).then(function() { console.log('Async: Copying to clipboard was successful!'); }, function(err) { console.error('Async: Could not copy text: ', err); });"
#     Bookmarklet save as          : "javascript:var data=document.evaluate('.//*[@id="1"]/table/tbody/tr/td/div/div/div[2]', document.getElementsByTagName('iframe')[0].contentDocument, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerHTML; var a=document.createElement('a'); var b = new Blob([data], {type:'text/svg'}); var url = URL.createObjectURL(b); a.href = url; a.download = 'PGAdminExplainDiagram.svg'; a.click(); setTimeout(function(){URL.revokeObjectURL(url)}, 0)"
#
# 2.a If you used the copy bookmarklet create a file, paste the clipboard and save as <somefile>.svg
#
# 3. ensure the files are named 1 through 22.svg and are in a folder in which the appropriate "<pathToPythonEnvironment>/site-packages/pgadmin4-web/pgadmin/misc" folder is linked as misc.
#    This is necessary so that the script can find the relativley linked graphics
# 4. Run this script, it will:
#     embed the linked files
#     create pdf versions of the svg files

pictureDir='/home/ted/pgadmin4svg/';
filePrefix='pg10.3_tcph_query';

#run embedder script
for i in {1..22}; do
	python36 svgInliner.py ${pictureDir}${i}.svg ${pictureDir}${filePrefix}${i}.svg > /dev/null;
	echo "Processed ${i}";
done

#convert svg to pdfs
cmds=();

for i in {1..22}; do
	filebasename="${pictureDir}${filePrefix}${i}";
	cmds+=("${filebasename}.svg --export-pdf=${filebasename}.pdf");
done; 
printf -v cmds "%s\n" "${cmds[@]}"; 
cmds=${cmds%?}; 
echo "$cmds" | inkscape --shell
