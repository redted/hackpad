import sys
import re
from string import Template

class parseIterator:
    def __init__(self, regex, string):
        self.string = string
        self.regex  = regex
    
    def __iter__(self):
        self.pos = 0
        return self
    
    def __next__(self):
        if len(self.string) <= self.pos:
            raise StopIteration
        
        match = self.regex.match(self.string, self.pos)
        
        try:
            self.pos = match.end(0)
        except AttributeError as err:
            print("Attribute error, likely your regex could not match anything")
            print("Current Position:", self.pos)
            print("String:\n", self.string[self.pos: self.string.find("\n", self.pos)])
            
            sys.stdout.flush()
            
            raise
        
        return match
    
    #Python2 backwards compatibility
    def next(self):
        return self.__next__()

def parseFile(filename, prefix =""):
    with open(sys.argv[1], 'r') as f:
        source = f.read()
    
    parseString(source, prefix)

def parseString(source, prefix =""):
    regexPort = re.compile(
                            r"""
                                ^\s*
                                (?:
                                    (?P<portDefinition>
                                        (?P<portDirection>input|output)\s*
                                        (?P<portType>logic)?
                                        (?P<dimensionsPacked>(?:\[\s*\d+\s*:\s*\d+\s*\]\s*?)*)\s*
                                        (?P<portName>[A-z][0-9A-z_]*)\s*
                                        (?P<dimensionsUnpacked>(?:\[\s*\d+\s*:\s*\d+\s*\])*)\s*
                                        (?P<seperator>,?)\s*
                                    )
                                    |(?P<UNKNOWN>.+)
                                    |
                                )
                                $\r?\n?
                            """,
                             re.MULTILINE | re.VERBOSE)
    
    inputPorts  = 0
    outputPorts = 0
    
    inputPortlist  = ""
    outputPortlist = ""
    
    inputPortnames  = []
    outputPortnames = []
    
    outputDevectorize = ""
    inputDevectorize  = ""
    
    indent = "    "
    wirePostfix = "_helperWire_SCRIPTED"
    
    class bundler(object):
        pass
    
    tunneler = bundler()
    tunneler_wires    = ""
    tunneler.inVec    = prefix + "input_vectorizer inVectorizer(\n"
    tunneler.inDeVec  = prefix + "input_devectorizer inDevectorizer(\n"
    tunneler.outVec   = prefix + "output_vectorizer outVectorizer(\n"
    tunneler.outDeVec = prefix + "output_devectorizer outDevectorizer(\n"
    tunneler.target   = "YOUR_MODULE USER_INSTANCE(\n"
    
    for port in parseIterator(regexPort, source):
        #print(a.groups())
        #print("G:", port.group("UNKNOWN"))
        if port.group("portDefinition"):
            packedDims   = port.group("dimensionsPacked") or ""
            unpackedDims = port.group("dimensionsUnpacked") or ""
            portDir      = port.group("portDirection")
            portName     = port.group("portName")
            portType     = port.group("portType") or ""
            
            dimension = parseDimensions(packedDims)*parseDimensions(unpackedDims)
            
            tunneler_wires  += "logic " + packedDims + " " + portName + wirePostfix + " " + unpackedDims + ";\n"
            tunneler.target += indent + "." + portName + "(" + portName + wirePostfix + "),\n"
            
            if portDir == 'input':
                inputDevectorize += indent + "assign " + portName + " = vector[" + str(inputPorts + dimension - 1) + ":" + str(inputPorts) + "];\n"
                
                inputPorts     += dimension
                inputPortlist  += indent + portDir + " " + portType + packedDims + portName + unpackedDims + ",\n";
                inputPortnames.append(portName)
                
                tunneler.inVec   += indent + "." + portName + "(" + portName + "),\n"
                tunneler.inDeVec += indent + "." + portName + "(" + portName + wirePostfix + "),\n"
            elif portDir == "output":
                outputDevectorize += indent + "assign " + portName + " = vector[" + str(outputPorts + dimension - 1) + ":" + str(outputPorts) + "];\n"
                
                outputPorts    += dimension
                outputPortlist += indent + portDir + " " + portType + packedDims + portName + unpackedDims + ",\n";
                outputPortnames.append(portName)
                
                tunneler.outVec   += indent + "." + portName + "(" + portName + "),\n"
                tunneler.outDeVec += indent + "." + portName + "(" + portName + wirePostfix + "),\n"
            else:
                raise ValueError("Direction of port is not defined [" + portDir + "]")
    
    with open('template.sv', 'r') as template:
        vectorizeTemplate = template.read()
    
    with open(prefix + 'input_devectorizer.sv', 'w') as f:
        f.write(
            Template(vectorizeTemplate).substitute(
                {
                    "moduleName" : prefix + "input_devectorizer",
                    "ports"      : re.sub(r'^(\s*)input ', r'\1output ', inputPortlist, flags = re.MULTILINE) + indent + "input [" + str(inputPorts - 1) + ":0]vector",
                    "body"       : inputDevectorize
                }
            )
        );
    
    with open(prefix + 'output_devectorizer.sv', 'w') as f:
        f.write(
            Template(vectorizeTemplate).substitute(
                {
                    "moduleName" : prefix + "output_devectorizer",
                    "ports"      : outputPortlist + indent + "input [" + str(outputPorts - 1) + ":0]vector",
                    "body"       : outputDevectorize
                }
            )
        );
    
    inputPortnames.reverse()
    with open(prefix + 'input_vectorizer.sv', 'w') as f:
        f.write(
            Template(vectorizeTemplate).substitute(
                {
                    "moduleName" : prefix + "input_vectorizer",
                    "ports"      : inputPortlist + indent + "output [" + str(inputPorts - 1) + ":0]vector",
                    "body"       : "assign vector = {" + ', '.join(inputPortnames) + "};"
                }
            )
        );
    
    outputPortnames.reverse()
    with open(prefix + 'output_vectorizer.sv', 'w') as f:
        f.write(
            Template(vectorizeTemplate).substitute(
                {
                    "moduleName" : prefix + "output_vectorizer",
                    "ports"      : re.sub(r'^(\s*)output ', r'\1input ', outputPortlist, flags = re.MULTILINE) + indent + "output [" + str(outputPorts - 1) + ":0]vector",
                    "body"       : "assign vector = {" + ', '.join(outputPortnames) + "};"
                }
            )
        );
    
    tunneler_wires += "logic [" + str(inputPorts  - 1) + ":0] inVector"    + wirePostfix + ";\n"
    tunneler_wires += "logic [" + str(inputPorts  - 1) + ":0] inVectorDe"  + wirePostfix + ";\n"
    tunneler_wires += "logic [" + str(outputPorts - 1) + ":0] outVector"   + wirePostfix + ";\n"
    tunneler_wires += "logic [" + str(outputPorts - 1) + ":0] outVectorDe" + wirePostfix + ";\n"
    
    tunneler.inVec    += indent + ".vector(inVector"    + wirePostfix + ")\n"
    tunneler.inDeVec  += indent + ".vector(inVectorDe"  + wirePostfix + ")\n"
    tunneler.outVec   += indent + ".vector(outVector"   + wirePostfix + ")\n"
    tunneler.outDeVec += indent + ".vector(outVectorDe" + wirePostfix + ")\n"
    
    tunneler.target = tunneler.target[:-2] + "\n" # strip trailing ",\n" readd "\n"
    
    with open(prefix + 'tunneler.sv', 'w') as f:
        f.write(
            Template(vectorizeTemplate).substitute(
                {
                    "moduleName" : prefix + "tunneler",
                    "ports"      : source, #inputPortlist + outputPortlist,
                    "body"       : tunneler_wires + "\n" + ");\n".join(tunneler.__dict__.values()) + ");\n"
                }
            )
        );

def parseDimensions(dimensions):
    #if we get None
    if not dimensions:
        return 1
    
    regexDimension = re.compile("\[\s*(?P<rangeLeft>\d+)\s*:\s*(?P<rangeRight>\d+)\s*\]")
    
    factor = 1
    
    for dimension in regexDimension.finditer(dimensions):
        factor *= int(dimension.group("rangeLeft")) - int(dimension.group("rangeRight")) + 1
    
    return factor

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(
            """Usage py " + __file__ + " inputFile [prefix]
            input file must be a system verilog portfile (the portlist part of a module between the \"()\"), 
            the only type supported is logic, or no type. Each port must be on a seperate line.
            
            Example:
                input [7:0]s_axi_control_AWADDR,
                input s_axi_control_AWVALID,
                output s_axi_control_AWREADY,
                input [31:0]s_axi_control_WDATA"""
        )
    
    if len(sys.argv) >= 3:
        prefix = sys.argv[2]
    else:
        prefix = ""
        
    parseFile(sys.argv[1], prefix)
    print("Generated vectorizer/devectorizer modules.")