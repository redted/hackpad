#include <iostream>

using namespace std;

int main() {
  const int bitsPerByte = 8;
  cout << "#ifndef BYTEREVERSAL_H\n"
  "#define BYTEREVERSAL_H\n"
  "\n"
  "char bytesBitReversed[] = {\n\t0";
  
  for(int i=1; i < 1<<bitsPerByte; ++i) {
     unsigned char reversed = 0;

     for(int j = 0; j < bitsPerByte; ++j) {
	if( i&(1 << j)) {
	  reversed |= 1 << (bitsPerByte - 1 - j);
	}
     }

     cout << ", ";
     
     if(!(i%32)) {
       cout << "\n\t";
     }
     
     cout << (int)reversed;
  }

  cout << "\n};\n"
  "\n"
  "#endif\n";
  return 0;
}